
/* 
This is a C-program  which reads an integer from standard input, converts this 
integer to a character string and prints this string on standard output.
To solve this problem, we define a function itoa, which converts a number to a
character string (you can generate the string backwards and then reverse it).
itoa must accept three arguments: the number, the character string and a
minimum field width: the converted number must be padded with blanks on the 
left if necessary to make it wide enough. The minimum field with must be given
as an argument of the main program.
*/

#include <stdio.h>
#define LENGTH 15

main( int argc, char *argv[])
{
	int in, minwidth;
	char s[LENGTH];
	char *itoa();

	if (argc != 2) {
		fprintf(stderr, "Usage : %s <number>\n", argv[0]);
		exit(1);
	}

	minwidth = 0;
	for( ; *(argv[1]); argv[1]++)
		minwidth = minwidth * 10 + *(argv[1]) - '0';
	printf("minimal width = %d\n", minwidth);
	if (minwidth > LENGTH) {
                printf("max stringlength = %d\n", LENGTH);
                exit(1);
        }

        printf("give integer\n");
        if(scanf("%d", &in) == 0){
		printf("Bad value. Aborted.\n");
		exit(1);
	}

        printf("result %s\n", itoa(in, s, minwidth));

	exit(0);
}

