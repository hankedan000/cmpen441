/*  
itoa must accept three arguments: the number, the character string and a
minimum field width: the converted number must be padded with blanks on the 
left if necessary to make it wide enough. The minimum field with must be given
as an argument of the main program.
*/

#include "conv.include1.h"

void
reverse(char s[])
{
	int c, i, j;

	for(i = 0, j = strlen(s) - 1; i < j; i++, j--) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

char *
itoa(int n,char s[],int w)
{
	int i, sign;
	
	if ((sign = n) < 0)
		n = -n;
	i = 0;
	do {
		s[i++] = n % 10 + '0';
	} while ((n /= 10) > 0);

	if (sign < 0)
		s[i++] = '-';

	while(i < w)
		s[i++] = ' ';

	s[i]= '\0';

	reverse(s);
	return s;
}
