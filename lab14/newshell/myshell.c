#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <errno.h>

#define DEBUG

#define BUFFER_SIZE	256
#define REDIR_NONE	-1
#define REDIR_OUT	0
#define REDIR_IN	1
#define REDIR_APP_OUT	2
#define CMD		0
#define FILENAME	1

int prompt(const char * prompt, char ** line);
void rmchar(char * str, char garbage);
int tokarr(char * str, const char * delim, char **arr);
void redirect(int type, const char * filename);
int charcnt(char * str, char c){
	int cnt = 0;
	while(*str!='\0')
		cnt += (*(str++)==c);
	return cnt;
}

int main(){
	char * line;
	char * pipe_tok;
	char * temp;
	char * buf[BUFFER_SIZE];
	char * redir_tok[2];
	size_t len;
	int redir_type = REDIR_NONE;
	int child_cnt,pipe_cnt,i;
	int pipefd[BUFFER_SIZE][2];
	pid_t pid;

	// continue until user enters "exit"
	while(prompt("> ",&line)){
		/* SEE IF ITS A REDIRECTION */
		if(strstr(line,">>") != NULL){
			tokarr(line,">>",(char**)redir_tok);
			redir_type = REDIR_APP_OUT;
		} else if(strstr(line,"<") != NULL){
			tokarr(line,"<",(char**)redir_tok);
			redir_type = REDIR_IN;
		} else if(strstr(line,">") != NULL){
			tokarr(line,">",(char**)redir_tok);
			redir_type = REDIR_OUT;
		} else {
			redir_tok[CMD] = line;
			redir_type = REDIR_NONE;
		}// if
#ifdef DEBUG
		printf("CMD = %s\nFILENAME = %s\n",redir_tok[CMD],redir_tok[FILENAME]);
#endif
		/* TOKENIZE COMMANDS BY PIPE CHAR */
		child_cnt = 0;
		pipe_cnt = charcnt(line,'|');
		for(i=0;i<pipe_cnt;i++){
			pipe(pipefd[i]);
		}// for
		temp = redir_tok[CMD];// used for tokenizing the line
		// continue while there are more pipes
		while((pipe_tok=strtok(temp,"|"))!=NULL){
			temp = NULL;// so that strtok will get next token
			child_cnt++;
			/* FORK AND EXECUTE COMMAND */
			pid = fork();
			switch(pid){
			case 0:
				// child
#ifdef DEBUG
				printf("pipe_cnt=%d\n",pipe_cnt);
				printf("#%d's cmd: %s\n",child_cnt,pipe_tok);
				printf("child#%d: pipe[0]=%d\tpipe[1]=%d\n",child_cnt,pipefd[child_cnt-1][0],pipefd[child_cnt-1][1]);
#endif
				buf[tokarr(pipe_tok," ",(char**)buf)] = NULL;
				// are we piping commands?
				if(pipe_cnt>0){
					if(child_cnt==1){
						// first child in pipe
						dup2(pipefd[child_cnt-1][1],1);// pipe out
					} else if(child_cnt==pipe_cnt+1) {
						// last child in pipe
						printf("#%d in to fd%d\n",child_cnt,pipefd[child_cnt-2][0]);
						dup2(pipefd[child_cnt-2][0],0);// pipe in
					} else {
						// middle child in pipe
						dup2(pipefd[child_cnt-2][0],0);// pipe in
						dup2(pipefd[child_cnt-1][1],1);// pipe out
					}// if
					for(i=0;i<pipe_cnt;i++){
						close(pipefd[i][0]);
						close(pipefd[i][1]);
					}//for
				}// if
				// if last child
				if(child_cnt==pipe_cnt+1)
					redirect(redir_type,(const char *)redir_tok[FILENAME]);
				execvp((const char *)buf[0], buf);
				printf("unknown command '%s'.\n", buf[0]);
				return -1;
				break;
			default:
				// parent
//				close(pipefd[0]);
//				close(pipefd[1]);
				break;
			}// switch
		}// while
		for(i=0;i<pipe_cnt;i++){
			close(pipefd[i][0]);
			close(pipefd[i][1]);
		}// for
		// wait for all children to finish before prompting again
		for(i=0;i<pipe_cnt+1;i++){
			wait(NULL);
		}//for
		free(line);// release memory for next line
	}// while

	return 0;
}// end main

int prompt(const char * prompt, char ** line){
	size_t len = 0;

	/* PROMPT AND GET USER INPUT */
	printf("%s",prompt);
	getline(line,&len,stdin);// get a line from user
	rmchar(*line, '\n');// remove newline
	
	/* DID USER TYPE 'EXIT'? */
	return strcmp((const char *)*line,"exit");
}

void rmchar(char * str, char garbage){
	char *src, *dst;
	
	for (src = dst = str; *src != '\0'; src++) {
		*dst = *src;
		if (*dst != garbage) dst++;
	}// for
	
	*dst = '\0';
}

int tokarr(char * str, const char * delim, char **arr){
	size_t cnt = 0;
	char * temp;
	temp = strtok(str,delim);// get first token
	/* CHECK IS NULL */
	if(temp==NULL)
		return 0;
	/* GET SPACE FOR NEW INITIAL TOKEN */
	arr[cnt] = malloc(strlen(temp)*sizeof(char));
	strcpy(arr[cnt],temp);
	/* FIND OTHER TOKENS */
	while(1){
		cnt++;
		temp = strtok(NULL,delim);// get next token
		if(temp == NULL)
			return cnt;
		/* GET SPACE FOR NEW TOKEN */
		arr[cnt] = malloc(strlen(temp)*sizeof(char));
		strcpy(arr[cnt],temp);
	}// while
}

void redirect(int type, const char * filename){
	FILE * f;

	/* DO NOTHING IF THERE IS NO REDIRECTION */
	if(type == REDIR_NONE)	return;
	
	/* HANDLE REDIRECTION TYPES */
#ifdef DEBUG
	printf("reditection type is %d\n",type);
	printf("redirecting to %s\n",filename);
#endif
	switch(type){
	case REDIR_OUT:
		// open file descriptor
		if((f=fopen(filename,"w")) == NULL){
			// quit if there was a problem opening file
			printf("couldn't open %s... exiting\n",filename);
			exit(1);
		} else {
			// change stdout to file descriptor
			if(dup2(fileno(f),1)<0)
				printf("error while redirecting\n");
			fclose(f);
		}// if
		break;
	case REDIR_APP_OUT:
		// open file descriptor
		if((f=fopen(filename,"a")) == NULL){
			// quit if there was a problem opening file
			printf("couldn't open %s... exiting\n",filename);
			exit(1);
		} else {
			// change stdout to file descriptor
			if(dup2(fileno(f),1)<0)
				printf("error while redirecting\n");
			fclose(f);
		}// if
		break;
	case REDIR_IN:
		// open file descriptor
		if((f=fopen(filename,"r")) == NULL){
			// quit if there was a problem opening file
			printf("couldn't open %s... exiting\n",filename);
			exit(1);
		} else {
			// change stdin to file descriptor
			if(dup2(fileno(f),0)<0)
				printf("error while redirecting");
			fclose(f);
		}// if
	}// switch
}
