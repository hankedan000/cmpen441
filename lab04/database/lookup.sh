#!/bin/bash
#Academic Integrity Statement: I certify that, while others may have assisted me in brain storming, debugging and validating this program, the program itself is my own work. I understand that submitting code which is the work of other individuals is a violation of the course Academic Integrity Policy and may result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board. I also understand that if I knowingly give my original work to another individual that it could also result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board.
 
# Assisted by and Assisted line numbers:
# Your name: Daniel Hankewycz

# This is a database management script.
# Format of database entries:
#	<First Name>:<Last Name>:<Phone #>:<Address>
# The file must contain one entry per line.

#---------------------------- FUNCTIONS -----------------------------#
function printEntry {
	clear
	datafile=$1
	shift
	while test $# -gt 0; do
		echo "Entry #$1:"
		entry=$(sed "$1q;d" $datafile)
		colonCnt=$(echo $entry | grep -o "\:" | wc -l)
		# are all fields available to print
		if [ $colonCnt -eq 3 ]; then
			first=$(echo $entry | cut -f1 -d":")
			last=$(echo $entry | cut -f2 -d":")
			phone=$(echo $entry | cut -f3 -d":")
			address=$(echo $entry | cut -f4 -d":")
			echo "First Name: $first"
			echo "Last Name: $last"
			echo "Phone #: $phone"
			echo "Address: $address"
		else
			echo "this entry doesn't have sufficient info to print"
		fi
		echo # blank line
		shift
	done
	return 0
}

# Prompts user for first and last name of entry and returns a string containing
# the line numbers with those entries in the data file
#	$1 contains path to datafile
function findEntry {
	read -p "First Name: " first
	read -p "Last Name: " last 
	# search the database for entries with first AND last name
	foundEntries=$(grep -in "$first" $1 | grep -i "$last")
	# print a space delimited string containg line #s of found entries
	while read -r line; do
		echo -n "$(echo $line | cut -f1 -d":") "
	done <<< "$foundEntries"
	return 0 
}

# Prompts user for new entry info, and then adds it to the database file
#	$1 contains path to datafile
function addEntry {
	read -p "First Name: " first
	read -p "Last Name: " last
	read -p "Phone Number: " phone
	read -p "Address: " address
	if test $(grep -i "$first" $1 | grep -i "$last" | wc -l) -gt 0; then
		echo "That entry already exists"
	else
		echo "$first:$last:$phone:$address" >> $1
	fi
	return 0
}

# Prompts user for entry to delete and then removes it from database file
#	$1 contains path to datafile
function deleteEntry {
	datafile=$1
	set $(findEntry $datafile)
	while test $# -gt 0; do
		clear
		printEntry $datafile $1
		read -p "Delete entry #$1? (y/n): " response
		case $response in
			y | Y | yes | Yes)
				sed "$1d" $datafile > $datafile.tmp && mv $datafile.tmp $datafile
				;;
			n | N | no | No)
				;;
			*)
				echo "invalid entry"
				;;
		esac
		shift
	done
}

# Prompts user for entry info to display from a provided data file
# 	$1 contains path to datafile
function viewEntry {
	printEntry $1 $(findEntry $1)
	
	return 0
}

datafile=datafile # default to within working directory

##### PARSE ARGS #####
while test $# -gt 0; do
	case $1 in
		--help)
			echo "-------------lookup-------------"
			echo "lookup.sh [options]"
			echo "Options:"
			echo "  --help	: display this menu"
			echo "  -d | --data	: specify the datafile"
			shift
			;;
		-d | --data)
			# did they provide sufficient args for datafile path?
			if [ $# -ge 2 ]; then
				# if so, use it
				shift
				datafile=$1
			else
				# if not, display error, and quit
				echo "no datafile specified"
				exit 1
			fi
			# does the provided file exist?
			if [ ! -e $datafile ]; then
				echo "$datafile does not exist"
				exit 1
			fi
			shift
			;;
		*)
			echo "$1 is an unsupported arg"
			shift
			;;
	esac
done
echo "Opened $datafile for viewing/editing"

##### DISPLAY MENU #####
clear
while true; do
	echo "Database Management Menu"
	PS3='Please enter your choice: '
	options=("Add entry" "Delete entry" "View entry" "Exit")
	select opt in "${options[@]}"; do
		case $opt in
			"Add entry")
				addEntry $datafile
				break
				;;
			"Delete entry")
				deleteEntry $datafile
				break
				;;
			"View entry")
				viewEntry $datafile
				break
				;;
			"Exit")
				exit 0
				;;
			*)
				echo invalid option
				break
				;;
		esac
	done
done
exit 0
