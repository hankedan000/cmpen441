/*
Academic Integrity Statement: I certify that, while others may have assisted me in brain storming, debugging and validating this program, the program itself is my own work. I understand that submitting code which is the work of other individuals is a violation of the course Academic Integrity Policy and may result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board. I also understand that if I knowingly give my original work to another individual that it could also result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board.
 
 Assisted by and Assisted line numbers:
 Your name: Daniel Hankewycz
*/

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <utmp.h>
#include <time.h>

/******** FUNCTION PROTOTYPES ********/
unsigned long getFileSize(int fd);

int main(int argc, char **argv){
	/* MAKE SOME LOCAL VARIABLES */
	int utmp_fd = open("/var/run/utmp", O_RDONLY);
	struct utmp *ut;// array of utmp structs
	struct timeval *tv;// temp used to print time entry was made
	time_t temp_time;
	unsigned long utmp_size;
	int num_users, i;
	
	// exit program if we couldn't open utmp file
	if(utmp_fd<0){
		printf("couldn't open /var/run/utmp\n");
		return 1;
	}// if

	// Read in utmp structs into ut array
	utmp_size = getFileSize(utmp_fd);
	num_users = utmp_size/sizeof(*ut);
	ut = (struct utmp*)malloc((size_t)utmp_size);
	read(utmp_fd, (void *)ut, (size_t)utmp_size);

	for(i=0; i<num_users; i++){
		// Only show "normal" sessions
		if(ut[i].ut_type==7){
			// Get the UTC time of session
			// had to mask upper 32bits for 64bit machines
			tv = (struct timeval *)(&ut[i].ut_tv);
			temp_time = tv->tv_sec&0x00000000FFFFFFFF;
			printf("%s\t%s\t\t%s",
				ut[i].ut_user,
				ut[i].ut_line,
				ctime(&temp_time)
			);
		}// if
	}// for
	
	return 0;
}// end main

/******** FUNCTION IMPLEMENTATIONS ********/

/* getFileSize
Description: Computes the size of a file
Given:
	fd- file descriptor for the file of interest
Returns:
	file size in bytes
*/
unsigned long getFileSize(int fd){
	struct stat tmp_stat;
	fstat(fd, &tmp_stat);

	return tmp_stat.st_size;
}// end getFileSize(int fd)
