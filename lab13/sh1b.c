#include <stdio.h>
#include <signal.h>

void sint_handler(int dummy)
{
  signal(SIGINT, sint_handler);
  while(1) ;
}

void segv_handler(int dummy)
{
  signal(SIGINT, sint_handler);
  fprintf(stderr, "nfs server not responding, still trying\n");
  while(1) ;
}

main()
{

  char *s;

  signal(SIGSEGV, segv_handler);

  /* This should cause a segmentation violation: */
  s = (char *) 0;
  strcpy(s, "ms");

}

