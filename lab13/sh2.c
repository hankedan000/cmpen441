#include <signal.h>
#include <stdio.h>
#include <unistd.h>

int i, j, seconds;

void alarm_handler(int dummy)
{
  seconds++;
  printf("%d second%s just passed: j = %d.  i = %d\n", seconds,
     (seconds == 1) ? "" : "s", j, i);
}

main()
{

  seconds = 0;

  signal(SIGALRM, alarm_handler);
  alarm(1);
  pause();
  for (j = 0; j < 2000; j++) {
    for (i = 0; i < 1000000; i++);
  }
}



