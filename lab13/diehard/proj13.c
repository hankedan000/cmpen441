/*
Academic Integrity Statement: I certify that, while others may have assisted me in brain storming, debugging and validating this program, the program itself is my own work. I understand that submitting code which is the work of other individuals is a violation of the course Academic Integrity Policy and may result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board. I also understand that if I knowingly give my original work to another individual that it could also result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board.

See Academic Integrity Procedural Guidelines at:  https://psbehrend.psu.edu/intranet/faculty-resources/academic-integrity/academic-integrity-procedural-guidelines

 Assisted by and Assisted line numbers:

 Your name: Daniel Hankewycz
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

int lives = 5;// initial number of lives
int int_cnt = 0, quit_cnt = 0;

void sint_handler(int dummy){
	if(int_cnt<3){
		int_cnt++;
		lives--;
	}// if
}

void quit_handler(int dummy){
	if(quit_cnt<2){
		quit_cnt++;
		lives--;
	}// if
}

int main(){
	/* OVERRIDE DEFAULT SIGNAL HANDLERS */
	signal(SIGINT, sint_handler);
	signal(SIGQUIT, quit_handler);
	/* IGNORE ALL OTHER CATCHABLE SIGNALS */
	signal(SIGILL, SIG_IGN);
	signal(SIGABRT, SIG_IGN);
	signal(SIGFPE, SIG_IGN);
	signal(SIGSEGV, SIG_IGN);
	signal(SIGPIPE, SIG_IGN);
	signal(SIGALRM, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGTSTP, SIG_IGN);
	signal(SIGTTIN, SIG_IGN);
	signal(SIGTTOU, SIG_IGN);

	while(1){
		/* PRINT LIFE STATUS MESSAGE */
		switch(lives){
		case 5:
			printf("\r5 lives:   Going strong!\n");
			break;
		case 4:
			printf("\r4 lives:   Hit, still going strong!\n");
			break;
		case 3:
			printf("\r3 lives:   Wounded, still going!\n");
			break;
		case 2:
			printf("\r2 lives:   Damaged, still going!\n");
			break;
		case 1:
			printf("\r1 lives:    Dying, still going!\n");
			break;
		case 0:
			printf("\r0 lives:    Dead!\n");
			exit(0);
		default:
			printf("BAD LIVES CASE");
			break;
		}// switch
		
		/* PAUSE */
		sleep(1);
	}// while

	exit(EXIT_FAILURE);
}// end main
