#include	<stdio.h>
#include	<stdlib.h>
#include	<sys/types.h>
#include	<sys/stat.h>

/*
Academic Integrity Statement: I certify that, while others may have assisted me in brain storming, debugging and validating this program, the program itself is my own work. I understand that submitting code which is the work of other individuals is a violation of the course Academic Integrity Policy and may result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board. I also understand that if I knowingly give my original work to another individual that it could also result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board. 

See Academic Integrity Procedural Guidelines at:  https://psbehrend.psu.edu/intranet/faculty-resources/academic-integrity/academic-integrity-procedural-guidelines

 Assisted by and Assisted line numbers:
 Your name: Daniel Hankewycz
*/

/* FUNCTION PROTOTYPES */
void dostat(char *);
char getModeChar(struct stat *);
char *getOwnerPerm(struct stat *);
char *getGroupPerm(struct stat *);
char *getOtherPerm(struct stat *);

int main( int argc, char **argv){
	if(argc==1){
		printf("You must provide a file path as an arguement\n");
	} else {
		while(--argc)
			dostat(*++argv);
	}// if

	return 0;
}// end main

void dostat(char *filename){
	struct stat info;
	
	if ( stat(filename, &info) == -1 ){		/* cannot stat	 */
		perror( filename );			/* say why	 */
	} else {					/* else show info */
		printf("%s stats:\n", filename);
		printf("+-------------------------------+\n");
		printf("| mode | owner | group | others |\n");
		printf("| %c    ", (int) getModeChar(&info));	// mode
		printf("| %s   ", getOwnerPerm(&info));		// owner perm
		printf("| %s   ", getGroupPerm(&info));		// group perm
		printf("| %s    ", getOtherPerm(&info));	// other perm
		printf("|\n");
		printf("+-------------------------------+\n");
	}// if
}// end dostat

/*
 Returns a character that describes the to mode the file stat
 d for a directory
 - for a regular file
 c for a character-special device file
 b for a block-special device file
 l for a symbolic link
 p for a named pipe (a FIFO)
 s for a socket (flag is S_IFSOCK)
*/
char getModeChar(struct stat * info){
	static char lut[16] = {
		' ','p','c',' ',
		'd',' ','b',' ',
		'-',' ','l',' ',
		's',' ',' ',' '
	};

	return lut[info->st_mode>>12];
}

/* 
Returns a null terminated string with the owner permissions
r 	-> read permission
w 	-> write permission
x/s 	-> write permission/set UID enabled
*/
char *getOwnerPerm(struct stat * info){
	char *result = malloc(sizeof(char)*4);
	mode_t mode = info->st_mode;	

	/* CHECK READ PERMISSIONS */
	if((S_IRUSR&mode)==S_IRUSR)	result[0]='r';
	else				result[0]='-';
	/* CHECK WRITE PERMISSIONS */
	if((S_IWUSR&mode)==S_IWUSR)	result[1]='w';
	else				result[1]='-';
	/* CHECK EXE PERMISSIONS */
	if((S_IXUSR&mode)==S_IXUSR)	result[2]='x';
	else				result[2]='-';
	result[4]='\0';
	/* CHECK USER ID BIT */
	if((S_ISUID&mode)==S_ISUID)	result[2]='s';

	return result;
}

/* 
Returns a null terminated string with the group permissions
r 	-> read permission
w 	-> write permission
x/s 	-> write permission/set GID enabled
*/
char *getGroupPerm(struct stat * info){
	char *result = malloc(sizeof(char)*4);
	mode_t mode = info->st_mode;	

	/* CHECK READ PERMISSIONS */
	if((S_IRGRP&mode)==S_IRGRP)	result[0]='r';
	else				result[0]='-';
	/* CHECK WRITE PERMISSIONS */
	if((S_IWGRP&mode)==S_IWGRP)	result[1]='w';
	else				result[1]='-';
	/* CHECK EXE PERMISSIONS */
	if((S_IXGRP&mode)==S_IXGRP)	result[2]='x';
	else				result[2]='-';
	result[4]='\0';
	/* CHECK GROUP ID BIT */
	if((S_ISGID&mode)==S_ISGID)	result[2]='s';

	return result;
}

/* 
Returns a null terminated string with the other permissions
r 	-> read permission
w 	-> write permission
x/t 	-> write permission/sticky bit is set
*/
char *getOtherPerm(struct stat * info){
	char *result = malloc(sizeof(char)*4);
	mode_t mode = info->st_mode;	

	/* CHECK READ PERMISSIONS */
	if((S_IROTH&mode)==S_IROTH)	result[0]='r';
	else				result[0]='-';
	/* CHECK WRITE PERMISSIONS */
	if((S_IWOTH&mode)==S_IWOTH)	result[1]='w';
	else				result[1]='-';
	/* CHECK EXE PERMISSIONS */
	if((S_IXOTH&mode)==S_IXOTH)	result[2]='x';
	else				result[2]='-';
	result[4]='\0';
	/* CHECK STICKY BIT */
	if((S_ISVTX&mode)==S_ISVTX)	result[2]='t';

	return result;
}

/* SAMPLE RUN */
/*
daniel@danielhankewycz:~/cmpen441/lab06$ ./ls uid
uid mode stat:
+-------------------------------+
| mode | owner | group | others |
| -    | rws   | rw-   | r--    |
+-------------------------------+

daniel@danielhankewycz:~/cmpen441/lab06$ ./ls gid
gid mode stat:
+-------------------------------+
| mode | owner | group | others |
| -    | rw-   | rws   | r--    |
+-------------------------------+

daniel@danielhankewycz:~/cmpen441/lab06$ ./ls sticky 
sticky mode stat:
+-------------------------------+
| mode | owner | group | others |
| -    | rw-   | rw-   | r-t    |
+-------------------------------+

daniel@danielhankewycz:~/cmpen441/lab06$ ./ls /dev/null
/dev/null mode stat:
+-------------------------------+
| mode | owner | group | others |
| c    | rw-   | rw-   | rw-    |
+-------------------------------+
*/
