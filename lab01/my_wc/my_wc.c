#include<stdio.h>

int main(int argc, char **argv){
	if(argc==2){
		FILE *in;
		int c;// the character that is read
		int line_cnt = 0;// line count
		int char_cnt = 0;// character count
		int word_cnt = 0;// word count
		int prevCharWasWhiteSpace = 0;
		in = fopen(argv[1],"r");
		
		/* CHECK IF FILE EXISTS */
		if(in == NULL){
			printf("%s does not exist!\n", argv[1]);
			return 0;
		}// if

		/* GET FILE STATS */
		while((c = fgetc(in)) != EOF){
			switch(c){
			case '\n':
				line_cnt = line_cnt + 1;
				// count a new word when we hit NL
				if(!prevCharWasWhiteSpace)
					word_cnt = word_cnt + 1;
				prevCharWasWhiteSpace = 1;
				break;
			case '\r':
				// accommodate Windows new-line syntax
				break;
			case ' ':
			case '\t':
				if(!prevCharWasWhiteSpace)
					word_cnt = word_cnt + 1;
				prevCharWasWhiteSpace = 1;
				break;
			default:
				prevCharWasWhiteSpace = 0;
				break;
			}// switch
			char_cnt = char_cnt + 1;
		}// while
		
		/* PRINT FILE STATS */
		printf("File: %s\n", argv[1]);
		printf("Lines: %d\n", line_cnt);
		printf("Words: %d\n", word_cnt);
		printf("Characters: %d\n", char_cnt);
	} else {
		printf("no file specified, or too many arguements\n");
	}// if

	return 0;
}// end main()
