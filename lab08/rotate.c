#include <stdio.h>
#include <ctype.h>
#include <termios.h>

int main() {
	int c;
	struct termios original_mode;
	struct termios ttystate;

	// save off the original tty settings
	tcgetattr(0,&original_mode);

	// change settings
	tcgetattr(0,&ttystate);// read curr. setting
	ttystate.c_lflag  &= ~ICANON;     /* no buffering         */
	ttystate.c_cc[VMIN]     =  1;     /* get 1 char at a time */
	tcsetattr( 0 , TCSANOW, &ttystate);     /* install settings     */

	while ( (c=getchar() ) != EOF) {       /*  ctrl -d */
		if (c=='z')		c = 'a';
		else if (islower(c))	c++;
		putchar(c);
	}

	// restore initial terminal settings
	tcsetattr(0, TCSANOW, &original_mode); 

	return 0;
}
