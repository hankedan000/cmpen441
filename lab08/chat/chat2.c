#include <termios.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <curses.h>
#include <unistd.h>

#define USERNAME_LEN 16
#define PASSWORD_LEN 16

void getCreds(char user[USERNAME_LEN], char pass[PASSWORD_LEN]);
void initWindows(WINDOW ** in, WINDOW ** out);
void printOutputMsg(WINDOW * win, char * user, char * msg);
void writeInputWin(WINDOW * win, char c);
void getInputMsg(WINDOW * win, char * msg);

int main(){
	struct termios ini_sett, new_sett;
	char c;
	char user[USERNAME_LEN], pass[PASSWORD_LEN];
	char msg[80];
	int i;
	WINDOW *i_window_ptr;
	WINDOW *o_window_ptr;
	
	getCreds(user, pass);
	if(	strcmp((const char*)user,"daniel")==0 &&
		strcmp((const char*)pass,"xyz")==0 ){
	
		/* DISABLE ECHO AND ICANON */
		tcgetattr(0, &ini_sett);// get the initial settings	
		new_sett = ini_sett;
		new_sett.c_lflag &= ~(ECHO|ICANON);
		tcsetattr(0,TCSADRAIN,&new_sett);

		initWindows(&i_window_ptr, &o_window_ptr);

		printOutputMsg(o_window_ptr,user,"joined chat");
		i = 0;// restart the current msg char pointer
		while(1){
			c = getchar();
			mvwprintw(o_window_ptr,2,2,"%d",(int)c);
			switch(c){
			case '\r':
				getInputMsg(i_window_ptr, (char *)msg);
				printOutputMsg(o_window_ptr,user,(char *)msg);
				break;
			case 127:// delete key
				if(i>0)	i--;
				else 	i=0;
				msg[i] = '\0';
				break;
			default:
				msg[i] = c;
				msg[i+1] = '\0';// null terminate msg
				i++;
				mvwprintw(i_window_ptr,0,0,"%s",msg);
				wrefresh(i_window_ptr);
				break;
			}// switch
		}// while
		delwin(i_window_ptr);
		delwin(o_window_ptr);
		endwin();
	} else {
		printf("\nSorry, but we do not recognize those credentials.\n");
	}// if

	exit(0);
}

void getCreds(char user[USERNAME_LEN], char pass[PASSWORD_LEN]){
	struct termios ini_sett, new_sett;
	char c;
	int i;

	printf("Username: ");
	/* GET THE USER NAME AFTER USER PRESSES ENTER */
	i = 0;
	while(c=getchar()){
		if(c=='\n'){
			break;
		} else if(i>=(USERNAME_LEN-1)){
			printf("Sorry, than user name is too long");
			break;
		} else {
			user[i] = c;
			i++;
		}// if
	}// while	
	user[i] = '\0';

	printf("Password: ");
	/* GET THE USER'S PASSWORD AFTER THEY HIT ENTER */
	i = 0;
	tcgetattr(0, &ini_sett);// get the initial settings	
	new_sett = ini_sett;
	new_sett.c_lflag &= ~(ECHO|ICANON);
	tcsetattr(0,TCSADRAIN,&new_sett);
	while(1){
		if(c = getchar()){
			if(c=='\n'){
				break;
			} else if(i>=PASSWORD_LEN-1) {
				printf("Sorry, password is too long");
				break;
			} else {
				pass[i] = c;
				i++;
				printf("%c",'*');
			}// if
		}// if
	}// while
	pass[i] = '\0';
	tcsetattr(0,TCSADRAIN,&ini_sett);// restore initial tty settings
}// end getCreds()

void initWindows(WINDOW ** in, WINDOW ** out){
	int MaxX,MaxY;
	WINDOW * TEMP;

	initscr();
		
	/* SETUP SUB WINDOWS */
	getmaxyx(stdscr, MaxY, MaxX);
	*in = newwin((MaxY/2)-2,MaxX-2,MaxY/2+1,1);
	*out = newwin((MaxY/2)-2,MaxX-2,1,1);
	scrollok(*out,true);
	wrefresh(*in);
	wrefresh(*out);
	
	/* DRAW THE BORDERS */
	TEMP = newwin(MaxY/2,MaxX,0,0);
	box(TEMP,'|','-');
	wrefresh(TEMP);
	delwin(TEMP);
	TEMP = newwin(MaxY/2,MaxX,MaxY/2,0);
	box(TEMP,'|','-');
	wrefresh(TEMP);
	delwin(TEMP);
}// end initWindows()

void printOutputMsg(WINDOW * win, char * user, char * msg){
	int height, width;
	getmaxyx(win,height,width);
	wscrl(win,1);// scrol the window up 1 line
	mvwprintw(win,height-1,0,"%s: %s",user,msg);// display the message
	wrefresh(win);// update
}// end printOutputMsg()

void writeInputWin(WINDOW * win, char c){
	int x,y;

	// is this a backspace
	if(c==127){
		getyx(win,y,x);
		mvwdelch(win,y,x-1);
	} else {
		waddch(win, c);
	}// if
	wrefresh(win);
}// writeInputWin()

void getInputMsg(WINDOW * win, char * msg){
	wscanw(win,"%c",*msg);
	strcpy(msg,"copied string");
}// end getInputMsg()
