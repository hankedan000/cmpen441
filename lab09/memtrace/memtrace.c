#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int topoftext = 't';
extern char *edata, etext;

int main(){
	long int size;
	printf("begin of data = %p\n", &topoftext);	
	printf("end of data = %p\n", &edata);
	size = (long int)&edata;
	size -= (long int)&topoftext;
	printf("size of data = %lu\n\n", ((long int)&edata)-((long int)&topoftext));

	printf("begin of text %p\n", &etext);
	printf("end of text 0x%x\n", 0x08048000);
	printf("size of text %lu\n", ((long int)&etext)-0x08048000);

	return 0;
}// end main
