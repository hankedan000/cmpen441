#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(){
	char *bottom = (char *)sbrk(0);// bottom of heap
	char *newtop = 0;
	void *oldaddr;
	long int inc = 0x10000000000;

	while(inc>=1){
		oldaddr = sbrk(0);
		// see if we will cross boundary if done one more time
		while(sbrk(inc)==-1){
			brk(oldaddr);// go back
			if(inc>=1)
				inc/=2;
		}// if
		printf("%p\tinc %lu\n", oldaddr, inc);
	}// while

	return 0;
}// end main
