#include <stdio.h>
#include <stdlib.h>

int stackfuck(char *bottom){
	char * newtop = alloca(0);

	printf("bot %p\n", bottom);
	printf("top %p\n", newtop);
	printf("dif %p\n", (void *)(bottom-newtop));

	return stackfuck(bottom);
}

int main(){
	char bottom;// used just to reference the bottom of the stack

	stackfuck(&bottom);

	return 0;
}// end main
