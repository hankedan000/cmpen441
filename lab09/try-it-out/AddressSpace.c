/*  AddressSpace.c - Discover the address space layout */

#include <stdio.h>
#include <stdlib.h>
#include <alloca.h>
#include <unistd.h>

extern char etext, edata, end;
int a = 0xaaaa, b;

int main(int argc, char * argv[]) {
    int i, c = 0xcccc;
    int *d_ptr = (int*) malloc(sizeof(int));
    int *e_ptr = (int*) alloca(sizeof(int));
    b = 0xbbbb;
    *d_ptr = 0xdddd;
    *e_ptr = 0xeeee;
    printf("%p:main\n", &main);
    printf("%p:etext\n\n", &etext);
    printf("%p:a=%0x\n", &a, a);
    printf("%p:edata\n\n", &edata);
    printf("%p:b=%0x\n", &b, b);
    printf("%p:end\n\n", &end);
    printf("%p:d=%0x\n", d_ptr, *d_ptr);
    printf("%p:brk\n\n", sbrk(0));
    for(i = 1; i <= 8192; i++) {
        d_ptr = (int*) malloc(sizeof(int));
        *d_ptr = 0xdddd;
    }
    printf("%p:d=%0x\n", d_ptr, *d_ptr);
    printf("%p:brk\n\n", sbrk(0));
    printf("%p:e=%0x\n", e_ptr, *e_ptr);
    printf("%p:argc=%0x\n", &argc, argc);
    printf("%p:c=%0x\n", &c, c);
    return 0;
}
