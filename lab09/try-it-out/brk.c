#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
	intptr_t oldendds;
	intptr_t newendds;
	int increase = 200;
	int nothing = 0;
	int *pin = &increase;
	int returnvalue;
	extern int etext, edata, end;

	printf("etext = 0x%x (%u)\n", &etext, &etext);
	printf("edata = 0x%x (%u)\n", &edata, &edata);
	printf("end = 0x%x (%u)\n", &end, &end);

	oldendds = (intptr_t) sbrk(increase);
	printf("oldendds = 0x%x (%u)\n", oldendds, oldendds);
	newendds = (intptr_t) sbrk(nothing);
	printf("after increasing 200, newendds = 0x%x (%u)\n", newendds, newendds);
	printf("newendds + 16 = 0x%x (%u)\n", newendds + 16, newendds + 16);
	returnvalue = brk((void *)(newendds + 16));
	printf("returnvalue should be 0; it is %d\n", returnvalue);
	newendds = (intptr_t) sbrk(nothing);
	printf("after adding 16, newendds = 0x%x (%u)\n", newendds, newendds);
	return 0;
}
