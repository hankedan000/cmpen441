/* CMPSC 474 /CMPEN 441
 * Lab 2
 Academic Integrity Statement: I certify that, while others may have assisted me in brain storming, debugging and validating this program, the program itself is my own work. I understand that submitting code which is the work of other individuals is a violation of the course Academic Integrity Policy and may result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board. I also understand that if I knowingly give my original work to another individual that it could also result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board.
 
 Assisted by and Assisted line numbers:
 Your name: Daniel Hankewycz
 Your section: T 12:00-1:50pm
 */

#include <stdio.h>
#include <assert.h>

struct numlist {
	float *list;   /* points to list of numbers in an array */
	int   len;     /* number of items in list   */
	float min,     /* the minimal value in list */
	max,     /* the maximal value in list */
	avg;     /* the mean of the numbers   */
};

void compute_stats(struct numlist *lp){
	int i; // counter
	float *list = lp->list;// get pointer to array list
	if(i>0){
		float min = list[0];// initialize the min to the first element
		float max = list[0];// initialize the max to the first element
		float sum = 0;
		/* GET THE STATS FROM THE LIST */
		for(i=lp->len; i>=0; i=i-1){
			if(list[i]>max)// check for new max
				max = list[i];
			if(list[i]<min)// check for new min
				min = list[i];
			sum = sum + list[i];// accumulate sum
		}// for
		/* SET STATS IN THE STRUCT */
		lp->min = min;
		lp->max = max;
		lp->avg = sum/lp->len;
	}// if
}

/**
 * Outputs all of the data from the numlist
 */
void print_nums(struct numlist *lp)
{
	int i;// counter
	/* DISPLAY THE LIST */
	printf("numlist::list = {");
	for(i=0; i<lp->len; i++){
		printf(" %0.6f", lp->list[i]);
		// should I print a comma for intermediate element?
		if(i!=lp->len-1)
			printf(",");
	}// for
	printf(" }\n");
	/* DISPLAY LIST LENGTH */
	printf("numlist::len = %d\n", lp->len);
	/* DISPLAY LIST MIN */
	printf("numlist::min = %0.6f\n", lp->min);
	/* DISPLAY LIST MAX */
	printf("numlist::max = %0.6f\n", lp->max);
	/* DISPLAY LIST AVERAGE */
	printf("numlist::avg = %0.6f\n", lp->avg);
}

int main(int argc, char **argv)
{

	/* Build a simple numlist to test */
	float data[10] = { 0.1, 1.1, 2.1, 3.1, 4.1, 5.1, 6.1, 7.1, 8.1, 9.1 };
	struct numlist l;

	l.list = data;
	l.len = 10;

	compute_stats(&l);

	print_nums(&l);

	return 0;
}

/*
SAMPLE RUN:
	numlist::list = { 0.100000, 1.100000, 2.100000, 3.100000, 4.100000, 5.100000, 6.100000, 7.100000, 8.100000, 9.100000 }
	numlist::len = 10
	numlist::min = 0.000000
	numlist::max = 9.100000
	numlist::avg = 4.599999
*/
