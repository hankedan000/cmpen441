#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <time.h>
#include <dirent.h>

/*
Academic Integrity Statement: I certify that, while others may have assisted me in brain storming, debugging and validating this program, the program itself is my own work. I understand that submitting code which is the work of other individuals is a violation of the course Academic Integrity Policy and may result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board. I also understand that if I knowingly give my original work to another individual that it could also result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board.

See Academic Integrity Procedural Guidelines at:  https://psbehrend.psu.edu/intranet/faculty-resources/academic-integrity/academic-integrity-procedural-guidelines

 Assisted by and Assisted line numbers:
 Your name: Daniel Hankewycz
*/

void dostat(char *);
char getModeChar(struct stat *);
char *getOwnerPerm(struct stat *);
char *getGroupPerm(struct stat *);
char *getOtherPerm(struct stat *);

int main( int argc, char **argv){
	DIR * dirp;
	struct dirent * dp;
	char buf[256];
	
	if(argc==1){
		printf("You must provide a file path as an arguement\n");
		return 1;
	}// if

	while(--argc){
		argv++;// goto next arguement
		// is the arg a directory
		if((dirp = opendir(*argv))!=NULL){
			// go through each entry in the directory
			while((dp = readdir(dirp)) != NULL){
				if(strcmp(dp->d_name,".")!=0 && strcmp(dp->d_name,"..")!=0){
					sprintf(buf,"%s%s",*argv,dp->d_name);
					dostat(buf);
				}// if
			}// while
		} else {
			dostat(*argv);
		}// if
	}// while

	return 0;
}// end main

void dostat(char *filename){
	struct stat info;
	struct passwd * pw;
	struct tm * tm_ptr;
	char buf[256];

	if ( stat(filename, &info) == -1 ){		/* cannot stat	 */
		perror( filename );			/* say why	 */
	} else {					/* else show info */
		/* DISPLAY FILE PERMISSIONS */
		printf("%c", (int) getModeChar(&info));	// mode
		printf("%s", getOwnerPerm(&info));	// owner perm
		printf("%s", getGroupPerm(&info));	// group perm
		printf("%s ", getOtherPerm(&info));	// other perm

		/* DISPLAY LINK COUNT */
		printf("%3d ", (int)info.st_nlink);

		/* DISPLAY OWNER AND GROUP NAMES */
		pw = getpwuid(info.st_uid);		// get user info
		printf("%-8s ", pw->pw_name);		// display owner name
		pw = getpwuid(info.st_gid);		// get group info
		printf("%-8s ", pw->pw_name);		// display group name

		/* DISPLAY SIZE OF FILE IN BYTES */
		printf("%8d ", (int)info.st_size);

		/* DISPLAY THE DATE AND TIME */
		tm_ptr = localtime(&(info.st_mtime));
		strftime(buf, 256, "%b %e %I:%S", tm_ptr);
		printf("%s ", buf);

		/* PRINT FILE NAME */
		printf("%s\n", filename);
	}// if
}// end dostat

/*
 Returns a character that describes the to mode the file stat
 d for a directory
 - for a regular file
 c for a character-special device file
 b for a block-special device file
 l for a symbolic link
 p for a named pipe (a FIFO)
 s for a socket (flag is S_IFSOCK)
*/
char getModeChar(struct stat * info){
	static char lut[16] = {
		' ','p','c',' ',
		'd',' ','b',' ',
		'-',' ','l',' ',
		's',' ',' ',' '
	};

	return lut[info->st_mode>>12];
}

/* 
Returns a null terminated string with the owner permissions
r 	-> read permission
w 	-> write permission
x/s 	-> write permission/set UID enabled
*/
char *getOwnerPerm(struct stat * info){
	char *result = malloc(sizeof(char)*4);
	mode_t mode = info->st_mode;	

	/* CHECK READ PERMISSIONS */
	if((S_IRUSR&mode)==S_IRUSR)	result[0]='r';
	else				result[0]='-';
	/* CHECK WRITE PERMISSIONS */
	if((S_IWUSR&mode)==S_IWUSR)	result[1]='w';
	else				result[1]='-';
	/* CHECK EXE PERMISSIONS */
	if((S_IXUSR&mode)==S_IXUSR)	result[2]='x';
	else				result[2]='-';
	result[3]='\0';
	/* CHECK USER ID BIT */
	if((S_ISUID&mode)==S_ISUID)	result[2]='s';

	return result;
}

/* 
Returns a null terminated string with the group permissions
r 	-> read permission
w 	-> write permission
x/s 	-> write permission/set GID enabled
*/
char *getGroupPerm(struct stat * info){
	char *result = malloc(sizeof(char)*4);
	mode_t mode = info->st_mode;	

	/* CHECK READ PERMISSIONS */
	if((S_IRGRP&mode)==S_IRGRP)	result[0]='r';
	else				result[0]='-';
	/* CHECK WRITE PERMISSIONS */
	if((S_IWGRP&mode)==S_IWGRP)	result[1]='w';
	else				result[1]='-';
	/* CHECK EXE PERMISSIONS */
	if((S_IXGRP&mode)==S_IXGRP)	result[2]='x';
	else				result[2]='-';
	result[3]='\0';
	/* CHECK GROUP ID BIT */
	if((S_ISGID&mode)==S_ISGID)	result[2]='s';

	return result;
}

/* 
Returns a null terminated string with the other permissions
r 	-> read permission
w 	-> write permission
x/t 	-> write permission/sticky bit is set
*/
char *getOtherPerm(struct stat * info){
	char *result = malloc(sizeof(char)*4);
	mode_t mode = info->st_mode;	

	/* CHECK READ PERMISSIONS */
	if((S_IROTH&mode)==S_IROTH)	result[0]='r';
	else				result[0]='-';
	/* CHECK WRITE PERMISSIONS */
	if((S_IWOTH&mode)==S_IWOTH)	result[1]='w';
	else				result[1]='-';
	/* CHECK EXE PERMISSIONS */
	if((S_IXOTH&mode)==S_IXOTH)	result[2]='x';
	else				result[2]='-';
	result[3]='\0';
	/* CHECK STICKY BIT */
	if((S_ISVTX&mode)==S_ISVTX)	result[2]='t';

	return result;
}

/* SAMPLE RUN
daniel@danielhankewycz:~/cmpen441/lab07$ ./ls /
drwxr-xr-x  10 root     root         4096 Apr 17 11:23 /usr
-rw-------   1 root     root      5820896 Aug 11 12:54 /vmlinuz
drwxr-xr-x   2 root     root         4096 Apr 10 06:14 /mnt
drwxr-xr-x   3 root     root         4096 Apr 17 11:25 /media
drwxr-xr-x  97 root     root         4096 Oct 12 10:02 /etc
drwxr-xr-x   3 root     root         4096 Aug  1 07:20 /srv
drwx------   2 root     root        16384 Apr 17 11:13 /lost+found
-rw-r--r--   1 root     root     19347684 Sep 24 12:07 /initrd.img
drwxr-xr-x  14 root     root         3940 Oct 12 10:02 /dev
drwxr-xr-x   8 root     root         4096 Aug 27 04:58 /home
drwxr-xr-x   3 root     root         4096 Sep 24 12:07 /boot
drwxr-xr-x   2 root     root        12288 Sep 24 12:36 /sbin
drwxr-xr-x  13 root     root         4096 Aug  1 07:36 /var
drwxrwxrwt   2 root     root         4096 Oct 12 11:59 /tmp
drwxr-xr-x  21 root     root         4096 Apr 17 01:45 /lib
drwxr-xr-x   2 root     root         4096 Sep 24 12:41 /bin
drwxr-xr-x  21 root     root          700 Oct 12 10:57 /run
dr-xr-xr-x  13 root     root            0 Oct 12 10:56 /sys
drwxr-xr-x   2 root     root         4096 Apr 16 05:45 /opt
drwxr-xr-x   2 root     root         4096 May 12 07:03 /lib64
dr-xr-xr-x  88 root     root            0 Oct 12 10:54 /proc
drwx------   5 root     root         4096 Jul 23 12:15 /root
*/
