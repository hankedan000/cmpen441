#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/types.h>

#define APPLE 0
#define PEACH 1

void apple_func(void *);
void peach_func(void *);

sem_t sem;
char filename[256];

int main(){
	pthread_t thread1, thread2;

	// Get the filename
	printf("Please enter your data file: ");
	scanf("%s", (char *)&filename);

	// Initialize semaphore and create 2 data struct to send to pthreads
	sem_init(&sem,0,1);

	// create 2 pthreads
	pthread_create(&thread1, NULL, (void *)&apple_func, NULL);
	usleep(200);// to make thread 1 execute first
	pthread_create(&thread2, NULL, (void *)&peach_func, NULL);
	
	// wait for each to finish
	printf("waiting for thread1 to finish...\n");
	pthread_join(thread1, NULL);
	printf("waiting for thread2 to finish...\n");
	pthread_join(thread2, NULL);

	printf("Thread joined. Done!\n");

	return 0;
}// end main()

void apple_func(void * ptr){
	FILE * f;

	sem_wait(&sem);// wait for unlock
	printf("Thread 1: I am entering critical section\n");
	printf("**********Critical Section**************\n");
	
	f = fopen((const char *)&filename, "a");
	if(f){
		fprintf(f, "APPLE\n");
	}// if
	fclose(f);

	sleep(5);
	printf("Thread 1: I am exiting critical section\n");
	sem_post(&sem);// unlock semphore
}// end msg()

void peach_func(void * ptr){
	FILE * f;

	sem_wait(&sem);// wait for unlock
	printf("Thread 2: I am entering critical section\n");
	printf("**********Critical Section**************\n");

	f = fopen((const char *)&filename, "a");
	if(f){
		fprintf(f, "PEACH\n");
	}// if
	fclose(f);

	sleep(10);
	printf("Thread 2: I am exiting critical section\n");
	sem_post(&sem);// unlock semphore
}// end msg()
