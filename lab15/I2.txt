Daniel Hankewycz
Lab15
Short Answer:
1)	deadlock
2)	server
3)	binary semaphore
4)	critical region
5)	atomic operation

Multiple Choice:
1)	A
2)	A
3)	A
4)	A
5)	B
6)	B
7)	A
8)	A
9)	B
10)	B
11)	A
12)	A
