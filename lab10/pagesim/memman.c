#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "memman.h"

void reset(struct memman * mem){
	size_t len = (size_t)(mem->frame_c*sizeof(int));

	// re-allocate new memory
	if(mem->data_struct)
		mem->data_struct = malloc(len);
	else
		mem->data_struct = realloc(mem->data_struct, len);
	if(mem->mem)
		mem->mem = malloc(len);
	else
		mem->mem = realloc(mem->mem, len);
	memset(mem->data_struct, -1, len);
	memset(mem->mem, -1, len);
}// end reset()

// returns 1 if there was a page fault, 0 other wise
int fifoAdd(struct memman * mem, int addr){
	int i, page_to_remove, fault = 1;
	int * fifo = mem->data_struct;
	int page = addr/(mem->frame_s);// compute page that request is on
	
	// see if page is in memory
	for(i=0; i<(mem->frame_c); i++){
		if(fifo[i]==page){
			fault = 0;
			break;
		}// if
	}// for

	if(fault){
		page_to_remove = fifo[mem->frame_c-1];
		// bump up the array 1 element
		for(i=(mem->frame_c)-1; i>0; i--)
			fifo[i] = fifo[i-1];
		// add new page to data structure
		fifo[0] = page;

		// load the new page into memory
		for(i=0; i<mem->frame_c; i++){
			if(mem->mem[i]==page_to_remove){
				mem->mem[i] = page;
				break;
			}// if
		}// for

	}// if
	
	// display the simulated fifo structure
	printf("%c%X |\tmem: ", fault?'X':' ', page);
	printmem(mem);
	printf("\tfifo: ");
	printdatastruct(mem);
	if(fault){
		if(page_to_remove<0)
			printf("\tinit memory with %d\n", page);
		else
			printf("\tswap %X for %X\n", page_to_remove, page);
	} else {
		printf("\n");
	}// if

	return fault;
}// fifo(int * fifo, int frames, int addr)

// returns 1 if there was a page fault, 0 other wise
int lruAdd(struct memman * mem, int addr){
	int i, page_to_remove, fault = 1;
	int * fifo = mem->data_struct;
	int page = addr/(mem->frame_s);// compute page that request is on
	
	// see if page is in memory
	for(i=0; i<mem->frame_c; i++){
		if(fifo[i]==page){
			fault = 0;
			break;
		}// if
	}// for
	
	// Determine which page to switch out if there is a fault
	if(fault){
		page_to_remove = fifo[(mem->frame_c)-1];// used to swap later
		// load in the new page to the memory
		for(i=0; i<mem->frame_c; i++){
			if(mem->mem[i]==page_to_remove){
				mem->mem[i] = page;
				break;
			}// if
		}// for
		fifo[(mem->frame_c)-1] = page;// remove old from list
		i = (mem->frame_c)-1;	
	}// if

	// move element to front of list
	for(;i>0;i--)
		fifo[i] = fifo[i-1];
	fifo[0] = page;
	
	// display the simulated list structure and memory
	printf("%c%X |\tmem: ", fault?'X':' ', page);
	printmem(mem);
	printf("\tlist: ");
	printdatastruct(mem);
	if(fault){
		if(page_to_remove<0)
			printf("\tinit memory with %d\n", page);
		else
			printf("\tswap %X for %X\n", page_to_remove, page);
	} else {
		printf("\n");
	}// if

	return fault;
}// end lruAdd()

void printmem(struct memman * mem){
	int i;
	for(i=0; i<mem->frame_c; i++){
		if(mem->mem[i]<0)	printf("* ");
		else			printf("%X ", mem->mem[i]);
	}// for
}// end printmem()

void printdatastruct(struct memman * mem){
	int i;
	int * list = mem->data_struct;
	for(i=0; i<mem->frame_c; i++){
		if(list[i]<0)	printf("* ");
		else		printf("%X ", list[i]);
	}// for
}// end printdatastruct()
