#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "memman.h"

/*
Academic Integrity Statement: I certify that, while others may have assisted me in brain storming, debugging and validating this program, the program itself is my own work. I understand that submitting code which is the work of other individuals is a violation of the course Academic Integrity Policy and may result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board. I also understand that if I knowingly give my original work to another individual that it could also result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board.

See Academic Integrity Procedural Guidelines at:  https://psbehrend.psu.edu/intranet/faculty-resources/academic-integrity/academic-integrity-procedural-guidelines

 Assisted by and Assisted line numbers:

 Your name: Daniel Hankewycz
*/

int * mem,temp;

int main(int argc, char **argv){
	struct memman mem;
	int num, fault_cnt;
	float look_up_cnt;
	FILE * f;
	
	if(argc!=4){
		printf("Not enough arguments passed\n");
		printf("ex. ./sim sample.data 1000 4\n");
		exit(0);
	} else {
		// open the input file
		printf("opening %s\n", argv[1]);
		f = fopen(argv[1], "r");
		if(f == NULL){
			printf("Can't open %s... exiting\n", argv[1]);
			exit(0);
		}// if
		
		// get the page size
		if(sscanf(argv[2],"%d",&(mem.frame_s))==EOF){
			printf("INVALID PAGE SIZE!\n");
			exit(0);
		} else {
			printf("PAGE SIZE: %d\n", mem.frame_s);
		}// if

		// get the frame count
		if(sscanf(argv[3],"%d",&(mem.frame_c))==EOF){
			printf("INVALID FRAME COUNT!\n");
			exit(0);
		} else {
			printf("FRAME COUNT: %d\n", mem.frame_c);
		}// if
	}// if


	
	/********************************************/
	/*         PERFORM FIFO SIMULATION          */
	/********************************************/
	printf("\n\nPress ENTER to perform FIFO simulation...");
	getchar();
	reset(&mem);
	fault_cnt = 0;
	look_up_cnt = 0;
	// get user inputted address and compute the corresponding page
	while(fscanf(f,"%d",&num)!=EOF){
		fault_cnt += fifoAdd(&mem, num);
		look_up_cnt++;
		usleep(100000);// wait for 1 seconds
	}// while
	printf("Number of page faults = %d\n", fault_cnt);
	printf("Failure frequency = %0.2f%%\n", (100*fault_cnt)/look_up_cnt);

	
	/********************************************/
	/*          PERFORM LRU SIMULATION          */
	/********************************************/
	printf("\n\nPress ENTER to perform LRU simulation...");
	getchar();
	reset(&mem);
	fault_cnt = 0;
	look_up_cnt = 0;
	rewind(f);// go back to beginning of file
	// get user inputted address and compute the corresponding page
	while(fscanf(f,"%d",&num)!=EOF){
		fault_cnt += lruAdd(&mem, num);
		look_up_cnt++;
		usleep(100000);// wait for 1 seconds
	}// while
	printf("Number of page faults = %d\n", fault_cnt);
	printf("Failure frequency = %0.2f%%\n", (100*fault_cnt)/look_up_cnt);

	return 0;
}// end main
