#ifndef MEMMAN_H
#define MEMMAN_H

struct memman{
	int * mem;
	int * data_struct;
	int frame_c;
	int frame_s;
};
void reset(struct memman * mem);
int fifoAdd(struct memman * mem, int addr);
int lruAdd(struct memman * mem, int addr);
void printmem(struct memman * mem);
void printdatastruct(struct memman * mem);

#endif
