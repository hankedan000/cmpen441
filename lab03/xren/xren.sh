#!/bin/bash

#Academic Integrity Statement: I certify that, while others may have assisted me in brain storming, debugging and validating this program, the program itself is my own work. I understand that submitting code which is the work of other individuals is a violation of the course Academic Integrity Policy and may result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board. I also understand that if I knowingly give my original work to another individual that it could also result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board.
# 
# Assisted by and Assisted line numbers:
# Your name: Daniel Hankewycz

# VALIDATE ARGUEMENT COUNT
if [ $# -ne 2 ] && [ $# -ne 3 ]; then
	echo "Invalid number of arguements."
	echo "syntax is $ xren.sh [dest] old_extension new_extension"
	exit 1
fi

# GET THE DESTINATION AND EXTENSION TYPES
# did the user specify a destination?
if [ $# -eq 2 ]; then
	# if not, use the current working directory
	dest="$(pwd)/"
	old_ext=$1
	new_ext=$2
else
	# if yes, then get it from the arguements
	dest=$1
	# is dest a valid directory?
	if [ ! -d $dest ]; then
		# if not, display error and terminate
		echo "directory $dest does not exist"
		exit 1
	fi
	old_ext=$2
	new_ext=$3
fi

# BEGIN THE RENAMING PROCESS
for file in $dest*.$old_ext; do
	# VERIFY THAT FILE EXISTS
	[ ! -e "$file" ] && exit 0
	# GET FILE NAME WITHOUT DIRECTORY AND EXTENSION
	filename=$(basename "$file")
	filename="${filename%.*}"
	# GENERATE NEW FILE NAME
	new_file=$dest$filename.$new_ext
	# VERIFY THAT THE USER WANT TO RENAME THE FILE
	goodInput=0
	until [ $goodInput -eq 1 ]; do
		echo "rename $file? (y/n)"
		read varify
		# convert to lower case
		varify=$(echo $varify | tr "[:upper:]" "[:lower:]")
		case $varify in
			y | yes )	mv $file $new_file
						goodInput=1
						;;
			n | no )	# dont rename the file
						goodInput=1
						;;
			* )			echo "invalid input"
						;;
		esac
	done
done

exit 0
