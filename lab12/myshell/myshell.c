/*
Academic Integrity Statement: I certify that, while others may have assisted me in brain storming, debugging and validating this program, the program itself is my own work. I understand that submitting code which is the work of other individuals is a violation of the course Academic Integrity Policy and may result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board. I also understand that if I knowingly give my original work to another individual that it could also result in a zero credit for the assignment, or course failure and a report to the Academic Dishonesty Board.

See Academic Integrity Procedural Guidelines at:  https://psbehrend.psu.edu/intranet/faculty-resources/academic-integrity/academic-integrity-procedural-guidelines

 Assisted by and Assisted line numbers:

 Your name: Daniel Hankewycz
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define BUFFER_SIZE	256

void rmchar(char * str, char garbage);
int tokarr(char * str, const char * delim, char **arr);

int main(){
	char *line;
	char *buf[BUFFER_SIZE];
	size_t len;

	// continue until user enters "exit"
	while(1){
		/* PROMPT AND GET USER INPUT */
		printf(">");
		getline(&line, &len, stdin);// get a line from user
		/* DID USER TYPE 'EXIT'? */
		if(strcmp((const char *)line,"exit\n")==0)
			return 0;
		/* PARSE USER INPUT */
		rmchar(line, '\n');// remove newline
		buf[tokarr(line," ",(char**)buf)] = NULL;
		/* FORK AND EXECUTE COMMAND */
		switch(fork()){
			case 0:
				// child
				execvp((const char *)buf[0], buf);
				printf("unknown command '%s'.\n", buf[0]);
				return 0;
				break;
			default:
				// parent
				wait(NULL);// wait for child to finish
				break;
		}// switch
	}// while

	return 0;
}// end main

void rmchar(char * str, char garbage){
	char *src, *dst;
	
	for (src = dst = str; *src != '\0'; src++) {
		*dst = *src;
		if (*dst != garbage) dst++;
	}// for
	
	*dst = '\0';
}

int tokarr(char * str, const char * delim, char **arr){
	size_t cnt = 0;
	char * temp;
	temp = strtok(str,delim);// get first token
	/* CHECK IS NULL */
	if(temp==NULL)
		return 0;
	/* GET SPACE FOR NEW INITIAL TOKEN */
	arr[cnt] = malloc(strlen(temp)*sizeof(char));
	strcpy(arr[cnt],temp);
	/* FIND OTHER TOKENS */
	while(1){
		printf("tokenized %s\n", arr[cnt]);
		cnt++;
		temp = strtok(NULL,delim);// get next token
		if(temp == NULL)
			return cnt;
		/* GET SPACE FOR NEW TOKEN */
		arr[cnt] = malloc(strlen(temp)*sizeof(char));
		strcpy(arr[cnt],temp);
	}// while
}
