#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>


main()
{
  int i;
  int seekp;
  int fd;
  char *s1;
  char s2[1000];

  fd = open("tmpfile", O_WRONLY | O_TRUNC | O_CREAT, 0666);

  s1 = "Before forking\n";
  write(fd, s1, strlen(s1));

  i = fork();

  if (i > 0) {
    sleep(1);  /* Delay the parent by one second */
    s1 = "Parent";
  } else {
    s1 = "Child";
  }

  seekp = lseek(fd, 0, SEEK_CUR);
  sprintf(s2, "%s: After forking: Seek pointer = %d\n", s1, seekp);
  write(fd, s2, strlen(s2));
}
